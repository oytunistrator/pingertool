# pingertool

> Ping some server and email details. Command line results: [MM:SS:SS] <MESSAGE>
like about executation time.


## Help Parameters
```
php ping.php --help
```

## Help Results
```
 ping.php

Ping some server and email details. Command line results: [MM:SS:SS] <MESSAGE>
like about executation time.


-c/--count <argument>
     Ping Count (Default:10)


-h/--host <argument>
     Hostname


--help
     Show the help page for this command.


-i/--wait <argument>
     Wait Time (Default:10)


-m/--mail <argument>
     Reciever Address (Default:info@oytunozdemir.com)


-n/--name <argument>
     Reciever Name (Default:Oytun)


-p/--port <argument>
     Port (Default: 80)


-t/--timeout <argument>
     Timeout (Default:10)

```

## Edit Config
```
nano config.php
```

## Usage

```
php ping.php -h <host> -p <port> -c <count> -t <timeout> -i <wait time> -n <name> -m <mail> 
```

## Example
```
$ php ping.php -h oytunistrator.com
```

## Output
```
[00:00:01] ----------------------------
[00:00:01] Mail Details:
[00:00:01] ----------------------------
[00:00:01] Reciever Address: info@oytunozdemir.com
[00:00:01] Reciever Name: Oytun
[00:00:01] ----------------------------
[00:00:01] Host Details:
[00:00:01] ----------------------------
[00:00:01] Host: oytunistrator.com
[00:00:01] Port: 80
[00:00:01] Size (Bytes): 1024
[00:00:01] Count: 10
[00:00:01] Wait: 10
[00:00:01] Timeout (Seconds): 10
[00:00:01] Timeout (Seconds): 10
[00:00:01] ----------------------------
[00:00:01] Generated Command:
[00:00:01] ping -c 10 oytunistrator.com -p 80 -s 1024 -i 10 -t 10
[00:00:01] ----------------------------
[00:00:01] Starting proccess...
[00:10:01] Collecting results...
[00:10:01] Status: 0
[00:10:01] Results:
PATTERN: 0x80
PING sni.github.map.fastly.net (151.101.65.147): 1024 data bytes
1032 bytes from 151.101.65.147: icmp_seq=0 ttl=52 time=48.605 ms

--- sni.github.map.fastly.net ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 48.605/48.605/48.605/0.000 ms

[00:10:01] Emailing...
[00:10:01] Finising Process...
[00:10:01] Done.
[00:10:01] ----------------------------
```
## Incoming Mail (HTML Result)
```
Status: 0
Host/Domain: oytunistrator.com
Port: 80
Raw Results:
PATTERN: 0x80 PING sni.github.map.fastly.net (151.101.65.147): 1024 data bytes 1032 bytes from 151.101.65.147: icmp_seq=0 ttl=52 time=48.605 ms --- sni.github.map.fastly.net ping statistics --- 1 packets transmitted, 1 packets received, 0.0% packet loss round-trip min/avg/max/stddev = 48.605/48.605/48.605/0.000 ms
Total time: 00:10:01
```

## Incoming Mail (Raw Result)
```
Received: from mxfront13g.mail.yandex.net ([127.0.0.1])
    by mxfront13g.mail.yandex.net with LMTP id YaMoO2pq
    for <info@oytunozdemir.com>; Wed, 27 Sep 2017 17:53:22 +0300
Received: from n1nlsmtp03.shr.prod.ams1.secureserver.net (n1nlsmtp03.shr.prod.ams1.secureserver.net [188.121.43.193])
    by mxfront13g.mail.yandex.net (nwsmtp/Yandex) with ESMTPS id svJkOdh0vj-rLJaxHxj;
    Wed, 27 Sep 2017 17:53:21 +0300
    (using TLSv1.2 with cipher ECDHE-RSA-AES128-GCM-SHA256 (128/128 bits))
    (Client certificate not present)
Return-Path: api@mopaset.com
X-Yandex-Front: mxfront13g.mail.yandex.net
X-Yandex-TimeMark: 1506524001
Authentication-Results: mxfront13g.mail.yandex.net; spf=pass (mxfront13g.mail.yandex.net: domain of mopaset.com designates 188.121.43.193 as permitted sender, rule=[ip4:188.121.43.0/24]) smtp.mail=api@mopaset.com; dkim=neutral header.i=@mopaset.com
X-Yandex-Spam: 1
Received: from n1plcpnl0023.prod.ams1.secureserver.net ([46.252.205.149])
    by : HOSTING RELAY : with SMTP
    id xDhEda8rqraeuxDhEdQBDa; Wed, 27 Sep 2017 07:52:20 -0700
DKIM-Signature: v=1; a=rsa-sha256; q=dns/txt; c=relaxed/relaxed; d=mopaset.com
    ; s=default; h=Content-Transfer-Encoding:Content-Type:MIME-Version:Message-ID
    :Subject:From:To:Date:Sender:Reply-To:Cc:Content-ID:Content-Description:
    Resent-Date:Resent-From:Resent-Sender:Resent-To:Resent-Cc:Resent-Message-ID:
    In-Reply-To:References:List-Id:List-Help:List-Unsubscribe:List-Subscribe:
    List-Post:List-Owner:List-Archive;
    bh=UbBYXuO1R+pcERgY50bIudu7DwGo/WXSP+8wYYO2i7s=; b=EuVourYEPvPGBxzYDu1wcKzdKM
    d5J80T+hLSf1Ht5hdE5XGaJcRHJ8wMS2sI0LaVA3HdFam9gLODSqfAaa1cOI1bPUGHiCAiNtycd/B
    TMoeQXUvReKFPiYTVYwOQYHKn+wRsftWpNMSlXnQYinCI5kFt6mxfyxmq9sGLyUNGF1FJMjkC79bx
    hwFoaysCnc3uEgmlhWEZcWxDmnIsB72Fb2po+/utEH0pf3crCadVW6gNYVcA2xCj6FiUGvvgU7FkA
    lPnu0zCNpUZ7Wry7fvidLDcKx6bUrj4+qhLLOVMdT87/8KOTIq64nlcmZzmx7StHyIOa8NqUsDJLa
    gXi7h3sQ==;
Received: from posta.mopas.com.tr ([213.74.51.146]:53949 helo=Oytuns-MacBook-Pro.local)
    by n1plcpnl0023.prod.ams1.secureserver.net with esmtpsa (TLSv1.2:ECDHE-RSA-AES128-GCM-SHA256:128)
    (Exim 4.88)
    (envelope-from <api@mopaset.com>)
    id 1dxDhE-002Yap-5k
    for info@oytunozdemir.com; Wed, 27 Sep 2017 07:52:20 -0700
Date: Wed, 27 Sep 2017 14:52:19 +0000
To: Oytun <info@oytunozdemir.com>
From: MopasetAPI <api@mopaset.com>
Subject: Ping Status: ping -c 10 oytunistrator.com -p 80 -s 1024 -i 10 -t 10
Message-ID: <rwNU1K1XPq5iGvA8hMkHzqOkfh3tPKOO2pAczjM2U@Oytuns-MacBook-Pro.local>
X-Mailer: PHPMailer 6.0.1 (https://github.com/PHPMailer/PHPMailer)
MIME-Version: 1.0
Content-Type: multipart/alternative;
    boundary="b1_rwNU1K1XPq5iGvA8hMkHzqOkfh3tPKOO2pAczjM2U"
Content-Transfer-Encoding: 8bit
X-AntiAbuse: This header was added to track abuse, please include it with any abuse report
X-AntiAbuse: Primary Hostname - n1plcpnl0023.prod.ams1.secureserver.net
X-AntiAbuse: Original Domain - oytunozdemir.com
X-AntiAbuse: Originator/Caller UID/GID - [47 12] / [47 12]
X-AntiAbuse: Sender Address Domain - mopaset.com
X-Get-Message-Sender-Via: n1plcpnl0023.prod.ams1.secureserver.net: authenticated_id: api@mopaset.com
X-Authenticated-Sender: n1plcpnl0023.prod.ams1.secureserver.net: api@mopaset.com
X-Source: 
X-Source-Args: 
X-Source-Dir: 
X-CMAE-Envelope: MS4wfIRh5ngBcU1Z+m89GuB2UgRNTvIK5m6bmPZw90eQ/wZtT+NMwMjKtn1+FPKzxfM9J0x6SmOEgM2v4Uhsq1gcvj0tebRcqkI4hgi/egIDIa58JhJXjlfS
 XGrvt5Z+t2r/v40sH5kxWNrWdR6JKpOwo/NCmcL9SCtIs6pv5N8cDn6MLTb9irr4HLQ0Frqy29r+Mb7NIzFAQytFGt4mG0zEmik=
X-Yandex-Forward: cd3d8f961cc0448d71058c1f5dc631f7

This is a multi-part message in MIME format.
--b1_rwNU1K1XPq5iGvA8hMkHzqOkfh3tPKOO2pAczjM2U
Content-Type: text/plain; charset=us-ascii

&lt;b&gt;Status: &lt;/b&gt;0&lt;br&gt;&lt;b&gt;Host/Domain: &lt;/b&gt;oytunistrator.com&lt;br&gt;&lt;b&gt;Port: &lt;/b&gt;80&lt;br&gt;&lt;b&gt;Raw Results:&lt;/b&gt;&lt;br&gt;PATTERN: 0x80
PING sni.github.map.fastly.net (151.101.65.147): 1024 data bytes
1032 bytes from 151.101.65.147: icmp_seq=0 ttl=52 time=48.605 ms

--- sni.github.map.fastly.net ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 48.605/48.605/48.605/0.000 ms
&lt;br&gt;&lt;b&gt;Total time: &lt;/b&gt;00:10:01&lt;br&gt;

--b1_rwNU1K1XPq5iGvA8hMkHzqOkfh3tPKOO2pAczjM2U
Content-Type: text/html; charset=us-ascii

<b>Status: </b>0<br><b>Host/Domain: </b>oytunistrator.com<br><b>Port: </b>80<br><b>Raw Results:</b><br>PATTERN: 0x80
PING sni.github.map.fastly.net (151.101.65.147): 1024 data bytes
1032 bytes from 151.101.65.147: icmp_seq=0 ttl=52 time=48.605 ms

--- sni.github.map.fastly.net ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 48.605/48.605/48.605/0.000 ms
<br><b>Total time: </b>00:10:01<br>


--b1_rwNU1K1XPq5iGvA8hMkHzqOkfh3tPKOO2pAczjM2U--

```


## License

MIT © [Oytun OZDEMIR](https://bitbucket.org/oytunistrator/pingertool)

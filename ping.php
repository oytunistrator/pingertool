<?php
require_once 'vendor/autoload.php';
global $time_start;
$time_start = microtime(true); 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Commando\Command;
use PhpProc\Process;

try{
   $config = require('config.php'); 
}catch(\Exception $e){
    showDebug('Config Not Found: '. PHP_EOL . $e->getMessage(), 6);
}

global $messageArr;



$pingCommando = new Command(); 

$pingCommando->setHelp('Ping some server and email details. Command line results: [MM:SS:SS] <MESSAGE> like about executation time.');

$pingCommando->option('h')
                ->aka('host')
                ->describedAs('Hostname');

$pingCommando->option('p')
                ->aka('port')
                ->describedAs('Port (Default: 80)');

$pingCommando->option('c')
                ->aka('count')
                ->describedAs('Ping Count (Default:'.$config['count'].')');

$pingCommando->option('i')
                ->aka('wait')
                ->describedAs('Wait Time (Default:'.$config['wait'].')');

$pingCommando->option('t')
                ->aka('timeout')
                ->describedAs('Timeout (Default:'.$config['timeout'].')');

$pingCommando->option('m')
                ->aka('mail')
                ->describedAs('Reciever Address (Default:'.$config['to'].')');

$pingCommando->option('n')
                ->aka('name')
                ->describedAs('Reciever Name (Default:'.$config['name'].')');


function mailer($to, $name, $subject, $body, $config){
    $mail = new PHPMailer(true);  
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $config['mail']['host'];  // Specify main and backup SMTP servers
    $mail->SMTPAuth = $config['mail']['smtp'];                               // Enable SMTP authentication
    $mail->Username = $config['mail']['username'];                 // SMTP username
    $mail->Password = $config['mail']['password'];                           // SMTP password
    $mail->SMTPSecure = $config['mail']['secure'];
    $mail->CharSet = $config['mail']['charset'];                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = $config['mail']['port'];                                    // TCP port to connect to

    $mail->addAddress($to, $name);
    $mail->setFrom($config['mail']['from']['mail'], $config['mail']['from']['name']);     
    $mail->isHTML($config['mail']['html']);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AltBody = htmlspecialchars($body);

    return $mail->send();
}

function timeBug($time_start){
    $micro = (microtime(true) - $time_start);
    return date("i:s:m",$micro);
}

function showDebug($message, $type = 0){
    global $messageArr;
    global $time_start;
    $messageStyle = '['.timeBug($time_start).'] '.$message. PHP_EOL;
    $messageArr[] = $messageStyle;

    switch ($type) {
        case 1:
            print($messageStyle);
            break;
        case 2:
            print $messageStyle ;
            break;
        case 6:
            die($messageStyle);
            break;
        default:
            echo $messageStyle;
            break;
    }
    
}

if($pingCommando['host']){
    $port = $config['port'];
    $count = $config['count'];
    $size = $config['size'];
    $wait = $config['wait'];
    $timeout = $config['timeout'];
    $to = $config['to'];
    $name = $config['name'];

    if($pingCommando['port']){
        $port = $pingCommando['port'];
    }
    if($pingCommando['size']){
        $size = $pingCommando['size'];
    }
    if($pingCommando['count']){
        $count = $pingCommando['count'];
    }
    if($pingCommando['wait']){
        $wait = $pingCommando['wait'];
    }
    if($pingCommando['timeout']){
        $timeout = $pingCommando['timeout'];
    }
    if($pingCommando['mail']){
        $to = $pingCommando['mail'];
    }
    if($pingCommando['name']){
        $name = $pingCommando['name'];
    }

    showDebug('----------------------------');
    showDebug('Mail Details: ');
    showDebug('----------------------------');
    showDebug('Reciever Address: '.$to);
    showDebug('Reciever Name: '.$name);
    showDebug('----------------------------');
    showDebug('Host Details: ');
    showDebug('----------------------------');
    showDebug('Host: '.$pingCommando['host']);
    showDebug('Port: '.$port);
    showDebug('Size (Bytes): '.$size);
    showDebug('Count: '.$count);
    showDebug('Wait: '.$wait);
    showDebug('Timeout (Seconds): '.$timeout);
    showDebug('Timeout (Seconds): '.$timeout);
    showDebug('----------------------------');
    $pingCmdRaw = "ping -c ".$count." ".$pingCommando['host']." -p ".$port." -s ".$size." -i ".$wait." -t ".$timeout;
    showDebug('Generated Command: ');
    showDebug($pingCmdRaw);
    showDebug('----------------------------');
    showDebug('Starting proccess...');
    
    try{
        $process = new Process();
        $pingCommandoProcess = $process
            ->setCommand($pingCmdRaw)
            ->execute();

        showDebug('Collecting results... ');
        showDebug('Status: '.$pingCommandoProcess->getStatus());
        showDebug('Results: '.PHP_EOL.$pingCommandoProcess->getStdOutContents());
        if ($pingCommandoProcess->hasErrors()) {
            throw new \Exception($pingCommandoProcess->getStdErrContents(), 1);        
        } else {
            /*
            echo 'Output: ' . $result->getStdOutContents();
            echo 'Status: ' . $result->getStatus() . PHP_EOL;
            *
            */
           $result = "<b>Status: </b>" .$pingCommandoProcess->getStatus() . '<br>';
           $result .= "<b>Host/Domain: </b>" .$pingCommando['host']. '<br>';
           $result .= "<b>Port: </b>".$port . '<br>';
           /*
           $result .= "<b>Pinger Server Adr: </b>". getenv('SERVER_ADDR') . '<br>';
           $result .= "<b>Pinger Remote Name: </b>". getenv('SERVER_NAME') . '<br>';
           $result .= "<b>Pinger Remote Software: </b>". getenv('SERVER_SOFTWARE') . '<br>';
           $result .= "<b>Pinger Remote Request Time: </b>". getenv('REQUEST_TIME') . '<br>';
           */
           $result .= "<b>Raw Results:</b><br>" . $pingCommandoProcess->getStdOutContents() . '<br>'; 
           $result .= "<b>Total time: </b>". timeBug($time_start) . '<br>';
           if($config['debug']){
            $result .= "<b>Debug Command: </b><br>";
            foreach ($messageArr as  $message) {
                $result .= $message . "<br>";
            }
           }
            showDebug('Emailing... ');
            $mailer = mailer($to,$name, 'Ping Status: '.$pingCmdRaw, $result, $config);
            if($mailer){
                showDebug('Finising Process...');
            }else{
                throw new \Exception($mailer->ErrorInfo, 1);  
            }
        }
    }catch (\Exception $e) {
        if(!empty($e->getMessage())){
            showDebug('Error: '. PHP_EOL . $e->getMessage(), 1);
        }
    }
     showDebug('Done. ');
     showDebug('----------------------------');
}